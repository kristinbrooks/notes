# Command Line

## General

* `cmd-k` - clear entire terminal
* `ctrl-a` - beginning of line
* `ctrl-e` - end of line
* `man <command>` - manual for most commands (man page) 
* `--help` - after most commands to see its options
* `"space"` - delimiter between executable arguments
* `"something in quotes"` - sends it as a single argument, so there can be spaces
* `\" "` - escapes the space so it will still be a single argument 
* `echo` - echos a command back to the shell (you can use it with wildcards to preview what you will get from your
pattern match before potentially doing something destructive)
* `-flag` or `--flag` following a command they allow you to change a commands standard behavior, can be a single letter
or a word
* `history` - shows list of recent commands
* `atom .` `mate .` - opens current directory in Atom or Textmate respectively
* each user has their own permissions that determine what they are allowed to do
  * `whoami` - tells you who the current user is
  * root user is the top-level user --- they can create and delete other users
  * usually don't lof in as root user --- you use the command `sudo` (superuser do) --- allows you to use root user
  commands without compromising your system's security
* `source path_to_file` - executing the file, can use after updating a file without restarting your terminal

## Directories

* `~/`- home directory
* `/` - root directory
* `..` - parent directory
* `.` - current directory
* `cd somedir` - change to "somedir" directory
* `cd -` - change to the previous directory
* `mkdir` - make directory
* `mkdir -p` - will create a new path/tree of multiple directories
* `rmdir` - removes directory, but not a tree...must use `rm -r` for that
* `pwd` - print working directory

## Files

* `ls` -  list files
* `ls -l` - list files long
* `ls -al` - list files all, long
* `ls -G` - will highlight the directories in a color to separate them from files
* `cat` - print contents of a file
* `head` & `tail` - print the first or last lines of a file
* `head -n 5` - prints first 5 lines --- same thing can be done with `tail`
* `touch existing_file_name.txt` - updates the file's modified date 
* `touch new_file_name.txt` - makes new file
* `wc file_name` - get lines, words, characters, file name (with spaces separating them) --- can do multiple files
* `wc -l` - number of lines
* `rm` - remove (permanently...no trash, etc that can be undone)
* `rm -i` - prompts you before deleting each file
* `rm -f` - doesn't prompt you before deleting a write-protected file, does not display an error
 if the specified file doesn't exist
* `rm -r`/`rm -R` - permits recursive removal of directories and their contents
* `stat -x file_name ` - prints file info

## Copying and Moving

* `cp source destination` - copies a file from the source location to the destination location --- the source file must
exist, but the destination doesn't, a file with the name you give it will be created --- be careful, if you use an
existing destination name it overwrites whatever is there
* `cp file1 file2 destination_directory` - copy multiple files
* `cp -R` - copy directory and its contents (-R means recursively)
* `mv source destination` - moves a file/directory to the destination location
      
## Find

*
  
## Cut and Paste
 
* `cut -d, -f1 file` - `-d` is for a delimiter and `,` is the delimiter --- `-f` is for a field and `1`is the field you
want, can select multiple fields
* `paste` - pastes lines from multiple files together --- concatenates the same line numbers into one line

## Dates

* `date` - makes the Unix system time pretty and readable
* `date -r file_name` - gives you the last modification date of the file

## Symlinks

* `ln -s source symbolic_link` - creates a symlink between a source file/directory and the linked file/directory
* `ls -l symbolic_link` verifies the link was created
* to overwrite the destination path of the symlink, use the -f (--force) option
* to overwrite a symlink us `-f` (force) option `ln -sf my_file.txt my_link.txt` otherwise will get an error
* `unlink symlink_to_remove`
* `rm symlink_to_remove`
  * when removing (or un-linking) a symbolic link not append the / trailing slash at the end of its name
  * if you delete or move the source file to a different location, the symbolic file will be left dangling (broken)
  and should be removed
  
## Permissions

* `chmod` - stands for 'change mode' - it's the command to change access permissions of file system objects
(files and directories) --- for more info: https://en.wikipedia.org/wiki/Chmod

## Patterns

### Wildcards

* `ls d?g` - lists files that match the pattern d-any single character-g --- the ? is a wildcard
* `ls d*g` - lists files that match the pattern d-any number of characters-g --- the * is the wildcard --- can use
wildcards with any other commands (ls, cp, mv, etc.)

### Grep

* `grep pattern file` - returns line/s with matches...can use with wildcards, regular expressions, to find partial
matches, with spaces, etc. --- grep is case sensitive
* `grep -i` - ignores case
* `grep -o` - to only print the words that match your pattern
* `grep -c` - gives the number of matching lines
* `grep -c -v` - gives number of lines that don't match --- can do with one dash as well `-cv`

## Standard Input/Output and Pipes

* `> file_name` - redirects standard output from terminal to file
* `< filename` - redirects a file to standard in
* `paste <[echo hello) <[echo world)`, `paste <[cut -d, -f3 file_name) <[cut -d, -f1 file_name)` - executes the right
first and sends it to stdin --- second example lets you switch columns 1 and 3 which you can't do with `cut` alone
* `|` - pipe operator - connect stdout to stdin --- can string more than 2 commands together --- can concatenate 2 files
--- can pipe to sort

## Command Line Editors

### Vim

* `vim file_name` - opens file in vim or create the file in your directory if it doesn't exist
* starts in command mode which lets you issue command and do things like copy and paste (but not type)
* `i` - to get in insert(edit) mode
    * must be in insert mode to make changes (type)
* `esc` - to exit insert mode
* `/` to search
* `:` - to enter last line mode
* `wq` - to write (save) and quit
* `q`, or `x` - to quit
* "Vim is a powerful text editor that has hundreds of commands."

### Less

* `less file_name` ` opens file in less
* `/` - to search
* `q` - to quit

### Nano 

* easier to use than vim and less!!!
* `nano file_name.txt` -  takes you into the nano text editor with that file open
* shows common commands at bottom of the screen
* `ctrl-w` - search
* `ctrl-k` - cut line
* `ctrl-u` - uncut line (paste)
* `ctrl-o` - will ask you for the file output name
* `crtl-c` - cancel
