# Databases

## What is a Database?

* without a database, if server stops for any reason, all non-hardcoded data is lost
* a database is a collection of information/data that have an interface that allows it to be
 interacted with

## SQL vs NoSQL

(pronounced sequel)

### SQL
 
*relational databases
* been around the longest --- what most people think of when they think of databases
* tabular databases
* must define a table ahead of time --- not flexible
* flat
```
======================
A RELATIONAL DATABASE
======================

USERS TABLE
id | name  |  age  |  city        favColor
--------------------------
1  | Tim   |  57   |  NYC         null/false/nil/etc
2  | Ira   |  24   |  Missoula    purple
3  | Sue   |  40   |  Boulder     null/false/nil/etc


COMMENTS TABLE
id |        text
-------------------------------
1  |  "lol"
2  |  "Come to Montana!"
3  |  "I love puppies!!!"
4  |  "Seriously Montana is great!"


USER COMMENTS JOIN TABLE
userId  |  commentId
-----------------------
   1    |     3
   2    |     2
   2    |     4
   3    |     1
```
* every user entered must follow the 'USERS TABLE' pattern
    * we're defining users and then adding instances of users to the table
    * if wanted to add favorite color for one person, it would have to be added for everyone
* join tables relate the data from other tables

### NoSQL

* non-relational databases
* don't have to define patterns ahead of time --- much more flexible
* no tables
* things can be nested --- not flat
* not necessarily better than SQL databases, most times they're not, but in specific situations they
 make sense
```
==========================
A NON-RELATIONAL DATABASE
==========================

{
    name: 'Ira',
    age: 24,
    city: 'Missoula',
    commetns: [
        {text: 'Come visit Montana!'},
        {text: 'Seriously come visit Montana!!!'},
        {text: Why does no one care about Montana???'}
    ],
    favColor : 'purple'
}

{
    name: 'Tammy',
    age: 24,
    city: 'Missoula',
    commetns: [
        {text: 'Hi'},
    ],
    favFood: 'Ribeye'
}
```
* looks like javascript --- called BSON (Binary Javascript Object Notation)
    * basically javascript objects
* can can just add favorite color to Ira's object
* can have other objects with different contents like you can see in the comparison between Ira
 and Tammy

#### MongoDB

* a NoSQL database
* most popular with Node and Express
    * MEAN stack is: Mongo, Express, Angular, Node
    
##### Commands


* `mongod` - running MongoDB (instructions here are Mac specific)
    * [Docs](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/)
    * Option 1: run manually in the background
        * to start: `mongod --config /usr/local/etc/mongod.conf --fork`
        * to stop:  
    * Option 2: run (i.e. the mongod process) as a MacOS service using `brew` (recommended)
        * to start:`brew services start mongodb-community@4.4`
        * to stop: `brew services stop mongodb-community@4.4`
* `mongo` - opens the MongoDB shell
    * shell is good for testing things out and debugging, but not where we will write most of
     our code --- most will be written in Express code
* `help` - shows some basic features
* `show dbs` - shows database names
    * admin and local are the two default dbs it knows about
    * in TWDB we'll make a separate DB for each project to keep things self-contained
* `use` - type command and name of database --- if it exists it will use it --- if it doesn't
 exist it will make it and then use it
    * DBs won't show until they have something in them
    * we add things in by creating collections
* `insert` - don't have to create a new collection and then add to it, we can insert immediately
    * example: `> db.dogs.insert({name: 'Rusty', breed: 'Mutt'})`
        * `db` refers to the db we're on
        * `dogs` is the collection
        * after insertion, will get a `WriteResult`
* `show collections` - see all collections in DB
* `find` - to show items in collections --- also called retrieving or reading
    * items will have long unique `"_id"`s assigned by mongo 
    * `> db.dogs.find()` shows all the dogs in the collection
    * example: `> db.dogs.find({name: 'Rusty'})`
        * will find only the specific entries with dogs named 'Rusty'
* `update` - edit a previously entered entry
    * takes two things: something to select by, and thing to update
    * example: `> db.dogs.update({name: 'Rusty'}, {breed: 'Corgie')`
        * this updates but overwrites the whole entry, so the name is gone and the breed is now
         the only thing in that entry
    * example: `> db.dogs.update({name: 'Rusty'}, {breed: 'Corgie')`
        * `> db.dogs.update({name: 'Rusty'}, {$set: {name: 'Tater', isCute: true}})`
        * keeps name but changes it to 'Tater' and adds `isCute` to the entry --- but with the
         `$set`, the breed wasn't overwritten like the name was in the previous example
* `remove` - removes entries from the collection
    * will remove everything that matches
    * example: `> db.dogs.remove({breed: 'Mutt'})`
* `drop` - deletes a whole collection
    * example: `> db.dogs.drop()`
    * do occasionally when making big changes and have data you don't care about 
* a common acronym is CRUD - Create Read Update Destroy
    * in Mongo the CRUD commands are: insert, find, update, remove
    
##### Mongoose

* it is an ODM - Object Data Mapper
    * a way to write javascript in our files that will interact with our database
* MongoDB object modelling for Node.js
* "...provides a straight-forward, schema-based solution to model you application data. It
 includes built-in type casting, validation, query building, business logic hooks and more, out
  of the box." (from the [Mongoose Homepage](https://mongoosejs.com/))
* it is a package that makes it easier to interact with MongoDb inside our javascript files
    * can be done without it, but it makes it easier --- like jQuery makes it easier to interact
     with the DOM but we don't need it
* see example in repo: `the-web-developer-bootcamp/13_databases`
