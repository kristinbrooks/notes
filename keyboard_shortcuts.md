# Mac OS Keyboard Shortcuts

* `cmd-shift` - spotlight
* `cmd-a` - select all
* `cmd-n` - new window
* `cmd-t` - new tab
* `cmd-q` - quit window
* `cmd-w` - close tab
* `cmd-shift-[` - navigate left
* `cmd-shift-]` - navigate right
* `cmd-left` - beginning of line
* `cmd-right` - end of line
* `option-left/right` - skip words
* `cmd-option-esc` - force quit
* `cmd-l` - location bar
* `cmd-tab` - switch between open apps
* `cmd-backtick` - switch windows within an app
* `cmd-x` - cut
* `cmd-shift-.` - show hidden files in finder
* `cmd-ctrl-space` - emojis :) 
