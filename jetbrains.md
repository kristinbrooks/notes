# JetBrains

* `F1` - pulls up documentation
* project files should be in an `src` directory within the main project directory
* `option-enter` - shows replacement options
* `cmd-delete` - delete line
* `shift-tab` - removes extra tabs in front of code
* `option-cmd-L` - auto format
* `shift-F6` - to rename something in every location it's used within scope
* `cmd-shift-a` - search
* `cmd-/` - comment out highlighted code
* `ctrl-shift-r` - runs new program in IDE
* `ctrl-r` - runs last program that was run in IDE
* `cmd-d` - compares two files
* `ctrl -cmd-g` - selects all matching names with multiple cursors so they can all be edited at once

### Setting up new project in IntelliJ

* make sure file structure is right...must be src folder, etc.
* `cmd-;` - brings up the 'Project Structure' dialogue
* under 'Project' assign an SDK and set the project compiler output
* under 'Modules' under 'sources' tab mark the **src** as a Sources folder 

## Debugger
