* for full haskell setup instructions see : https://gitlab.com/thewoolleyman/haskell-project-setup

# Stack

## Installation
* `brew install haskell-stack`
*  * edit `~/.stack/config.yml` - enter name, email, and copyright,
[more info](https://docs.haskellstack.org/en/stable/yaml_configuration/#templates)

## Creating New Project

* `stack new my-haskell-project -p "category:MyProjectCategory"`
  * `MyProjectCategory` is your projects category
    * see [this](https://docs.haskellstack.org/en/stable/yaml_configuration/#templates)
    and [this](https://hackage.haskell.org/packages/) for category values
* from root of project`stack build --copy-compiler-tool hlint`
* debugging
  ```
  cd ../my-project
  stack install phoityne-vscode
  ```
* `stack build hoogle` - project docs setup, run from project directory
  * `stack hoogle --generate --local`
    * `stack hoogle --server --local --port=8080` - open at http://localhost:8080/

## Commands

### Build Project

* `stack build <target>` - to build a target
* `stack build` - builds the current project's targets, runs the GHC optimizer
* `stack build --fast` - disable optimizations to make it build faster
* `stack build --fast --test` or `stack test -- fast` are equivalent - run tests when building
* from lexi-lambda article:
  * `stack test --fast --haddock-deps --file-watch` - build and test project in background whenever code changes,
  adding `--file-watch` flag makes it easy to incrementally change project code and immediately see results
* from Chad's Haskell setup:
  * `stack test --fast --haddock-deps --copy-bins my-haskell-project`
    * Run it (should be in `~/.local/bin`, which should be on your path): `my-haskell-project-exe`

### haskell-linter

* `stack build --copy-compiler-tool hlint` - then set Preferences -> Settings -> Extensions -> haskell-linter -> 
Haskell > Hlint: Executable Path: /Users/ktown/.stack/compiler-tools/x86_64-osx/ghc-8.8.3/bin/hlint (use the 
appropriate path which you just installed there via --copy-compiler-tool - it must be absolute, can't use ~)
  * should have `stack-yaml-lock`, commit and push it

### Docs

* `stack haddock --open package_name` -  opens the local documentation for a particular package, opens in web browser
* `stack test --haddock` - builds documentation, sometimes takes a long time
* `stack test --haddock-deps` - builds documentation on dependencies
* `stack haddock --open package_name` -  opens the local documentation for a particular package, opens in web browser

### GHCi

* `stack ghci` - to run ghci
* `:q` - exit
