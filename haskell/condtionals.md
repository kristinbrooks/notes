# Haskell Conditionals

## if-then-else

* haskell doesn't have "if" statements ---  it has *if expressions* that work with the `Bool` datatype
* syntax: 
  ```
  if CONDITION
  then EXPRESSION_A
  else EXPRESSION_B
  ```
    * the `CONDITION` must evaluate to a `Bool` --- if it evaluates to `True` then `EXPRESSION_A
    ` is the result, otherwise it is `EXPRESSION_B`
* example:
  ```
  Prelude> t = "Truthin"
  Prelude> f = "Falsin"
  Prelude> if True then t else f
  "Truthin"                             // 'if True' evaluates to 'True', so it returns 't'
  Prelude> if False then t else f
  "Falsin"                              // 'if False' evaluates to 'False', so it returns the
  Prelude> :t if True then t else f     // 'else' value ('f')
  if True then t else f :: [Char]
  ```
