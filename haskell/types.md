# Haskell Types

## Basics

* also called datatypes
* allow us to classify and delimit data, thus restricting the forms of data our programs can process
* this makes for easier program maintenance
* when evaluated expressions reduce to values and every value has a type
    * every expression's type is known at compile time => safer code
* types group together a set of values that share something in common
    * can be abstract or specific
    * think about types as being like mathematical sets (Set theory is the study of mathematical
     collections of objects. It was a precursor to type theory.)
     
## Determining Type in GHCi

* you can use the `:t` or `:type` command followed by any valid expression, and it will tell us
 the type
    * examples:
      ```
      ghci> :t 'a'
      'a' :: Char
      ghci> :t True
      True :: Bool
      ghci> :t "HELLO!"
      "HELLO!" :: [Char]
      ghci> :t (True, 'a')
      (True, 'a') :: (Bool, Char)
      ghci> :t 4 == 5
      4 == 5 :: Bool
      ```
* putting the character in quotes tells GHCi it's a `Char` and not a variable
* must use double quotes to tell GHCi it's a `String`
* the `::` operator here is read as "has type of"
* functions also have types --- when writing them we can choose to give them an explicit type
 declaration
    * example:
      ```
      removeNonUppercase :: [Char] -> [Char]
      removeNonUppercase st = [c | c <- st, c `elem` ['A'..'Z']]
      ```
        * `[Char] -> [Char]` means that it takes one string as a parameter
         and returns another string as a result
    * example:
      ```
      addThree :: Int -> Int -> Int -> Int
      add Three x y z = x + y + z
      ```
        * takes 3 Ints and returns an Int
    * parameters and the return type are separated by `->` with the return
     type always coming last
    * `:t` works with functions too
        * if you are unsure what the return type should be you can write the
         function without it and then check it with `:t`     
     
## Data Declarations

* how datatypes are defined
* the **type constructor** is the name of the type and is capitalized
* when you ae reading or writing **type signatures** (the type level of your code), the type
 names or type constructors are what you use
* **data constructors** are the values that inhabit the type they are defined in
    * the values that show up in your code at the **term level** (the values as they
     appear in your code, or the values that your code evaluates to) instead of the type level
* for `Bool` there are only two data constructors because there are only two truth values
    * example of data declaration for `Bool`:
      ```
      data Bool = False | True
       --  [1]     [2] [3] [4]
      ``` 
      1. type constructor for datatype `Bool` --- this is the name of the type and shows up in
       type signatures
      2. data constructor for value `False`
      3. the pipe `|` indicates a **sum type** or logical disjunction (*or*) --- so a `Bool` is
       `True` *or* `False`
      4. data constructor for the value `True`
* data declarations don't always follow precisely the same pattern
    * some use conjunction (*and*) instead of disjunction
    * some type constructors and data constructors may have arguments
* but they will always follow this basic structure:
    * start with `data`
    * followed by the type constructor (the name of the type that will appear in type signatures)
    * then `=` to denote a definition
    * then data constructors 
* example of how parts ot the data declaration show up in our code using function `not`:
    ```
    Prelude> :t not
    not :: Bool -> Bool
    ```
    * so the type query makes reference to the type constructor (name)
    ```
    Prelude> not True
    False
    ```
    * when we use the function, we use the data constructors (values) ---  and the expression
     evaluates to another data constructor

## Datatypes

### Numeric Types

* all the following numeric types have a *type class* `Num`
* you can find the bounds of numeric types using `minBound` and `maxBound` from the `Bounded
` type class --- syntax `minBound :: type`
* **fixed-precision** (or fixed-point) have immutable numbers of digits assigned for
 the parts of the number before and after the decimal point
* **arbitrary precision** means it can be used to do calculations with a high degree of
 precision rather than being limited to a specific degree of precision 
* **floating-point arithmetic** can be difficult, so it's better to use fixed-precision types if
 possible
    * **floating-point** can shift how many bits it uses to to represent numbers before and after
     the decimal point --- this does mean that floating-point arithmetic violates dome common
      assumptions and should be used with great care 

#### Integral Numbers

* `Int` - fixed-precision integer (meaning it has a range)
    * being fixed can be useful in some applications, but usually `Integer` is preferred
    * there are related types such as `Int8` and `Int16`, etc
* `Integer` - integers that can be arbitrarily large or small numbers (i.e. not fixed-precision)
* `Word` - fixed-precision integer with the lowest value being zero (i.e. whole numbers)
    * upper bounds of `Word` types are about double that of the same size `Int` types because the
     bit used for the negative numbers in `Int` is now used for extra positive numbers

#### Fractional Numbers

* the notation `Fractional a =>` denotes a *type class constraint* --- the type variable `a`must
 implement the `Fractional` type class
    * `Fractional` requires types to already have an instance of `Num` type class => `Num` is a
     superclass of `Fractional` --- functions from `Num` can bu used with `Fractional` numbers
     , but functions from `Fractional` cannot be used by all `Num` types
* `Float` - single-precision floating-point numbers
* `Double` - double-precision floating-point numbers --- has twice as many bits with which to
 describe numbers as `Float` type
* `Rational`- fractional number that represents the ratio of two integers --- it is arbitrarily
 precise but not as efficient as `Scientific`
* `Fixed` - fixed-point (or fixed-precision) type that can represent varying numbers of decimal
 points depending on which you choose (e.g. `Fixed E2` can track up to 2 digits after tha decimal
  point) --- the `base` library provides `E0`, `E1`, `E2`, `E3`, `E6`, `E9`, and `E12` --- you
   can add custom resolutions if you need
* `Scientific` - space efficient and almost arbitrary precision --- represented using scientific
 notation --- stores the coefficient as an `Integer` and the exponent as an `Int` (this is why it
  is only almost arbitrary --- `Int` has a limit)
  
## Boolean Type

* any function that accepts `Bool` type must allow for the possibility of `True` or `False
` --- you cannot specify that it should only accept one specific value
* Boolean infix operators:
    * conjunction  is `&&`
    * disjunction is `||`
        * we can use these with `not` because it evaluates to a `Bool` value
