# Haskell Lists

(note: many of the functions are considered *unsafe* because they don't know
 how to handle empty lists and will instead throw exceptions --- there are
  ways to make safer versions of such functions that will cover all cases)

* Haskell lists are *homogeneous* data structures, which means they store
 several elements of the same type
* they use `[]` with `,` to separate list values
* strings are lists => `"hello"` is the same as `['h','e','l','l','o']`
* `[]` is empty list

## Concatenation

* uses `++` operator
    * examples: `[1,2,3,4] ++ [9,10,11,12]` returns `[1,2,3,4,9,10,11,12]`,
    `"hello" ++ " " ++ "world"` returns `"hello world"`, and `['w','o
    '] ++ ['o','t']` returns `"woot"`
* `++` always takes two lists => can't do `[1,2,3,4] ++ 5` 
* appending something to the end of the list must use `++` and can take a
 long time if the list is long
* adding to the beginning (prepending) can be done almost instantaneously with
 the`:` operator (called *cons* operator)
    * examples: `'A': " SMALL CAT"` returns `"A SMALL CAT"` , `5:[1,2,3,4,5
    ]` returns `[5,1,2,3,4]`, and `1:2:3:[]` is the same as `[1,2,3]`
    
## Accessing List Elements

* `!!` - indexing operator
* indices start at 0
* format: `list !! index`
    * example: `"Steve Buscemi" !! 6` returns `'B'`

## Lists Inside Lists

* lists can contain lists as elements and lists can contain lists that
 contain lists, etc
* lists within lists can be different lengths, but not different types

## Comparing Lists

* lists can be compared if the items they contain can be compared
* if using `<`, `>`, `<=`, and `>=` to compare two lists, they are compared
 in lexicographical order (meaning the first two list heads are compared, anf
  if they're equal, then the second elements are compared, etc until
   differing elements are found)
    * examples: `[3,2,1] > [2,1,0]` is `True`, `[3,4,2] > [3,4,3]` is `False
    `, `[3,4,2] == [3,4,2]` is `True`
* a nonempty is is always greater than an empty one

## Additional List Operations

* `head list` returns the first element
* `tail list` removed the head and returns everything after it
* `last list` returns the last element
* `init list` returns everything except its last element
* can"t use the 4 previous functions on an empty list --- this error not
 caught at compile time so be careful
* `length list` returns its length
* `null list` checks if the list is empty, returns a Boolean
* `reverse list` returns a list with the elements in reverse order
* `take number list` extracts the specified number of elements from the
 beginning of the list
    * examples: `take 3 [5,4,3,2,1]` returns `[5,4,3]`, `take 0 [6,6,6
    ]` returns `[]`
    * if you try to take more elements than are in the list it returns the
     whole list
* `drop number list` is the opposite of `take`
    examples: `drop 3 [8,4,2,1,5,6]` returns `[1,5,6]`, `drop 0 [1,2,3,4
    ]` returns `[1,2,3,4]`, and `drop 100 [1,2,3,4]` returns `[]`
* `maximum list` returns the largest element
* `minimum list` returns the smallest element
* `sum list` takes a list of numbers and returns their sum
* `product` takes a list of numbers and returns their product
* ```
  item `elem` list
  ``` 
  tells us if the item is an element of the list, returns a Boolean
    * usually written as an infix function because it's easier to read
* `zip firstList secondList` - produces a list of pairs --- it takes two lists then 'zips' them
 together into one list by joining the matching elements into pairs
    * example: `zip [1,2,3,4,5] [5,5,5,5,5]` returns `[(1,5),(2,5),(3,5),(4,5),(5,5)]`
    * each list can be of different types
    * if the lists aren't the same length, only as much of the longer list is used as is needed,
     and the rest is ignored    
    
## Ranges

* can be used so don't have to type out every element in a list composed of
 elements that can be *enumerated* (counted off in order)
    * format: `[firstElement..lastElement]`
    * examples: `[1..20]` is `[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
    ,19,20]`,`['a'..'z']` is`"abcdefghijklmnopqrstuvwxyz"`, `['K'..'Z']` is
     `"KLMNOPQRSTUVWXYZ"`
* can specify a step between items
    * format: `[firstElement, secondElement..lastElement]`
    * example: `[2,4..20]` is `[2,4,6,8,10,12,14,16,18,20]`
    * can only specify a single step size (i.e. no Fibonacci sequence, etc)
    * some sequences that aren't arithmetic can't be specified unambiguously
     by giving their first few terms
* to make a list that counts down you must use `[firstElement, secondElement
..lastElement]` (i.e. `[20,19..1]` not `[20..1]`)
* infinite lists can be made by not specifying an upper limit
    * Haskell won't try to evaluate the entire list immediately, it will wait
     to see which elements you need
    * example: you can make a list of the first 24
     multiples of 13with `[13,26..24*13]`, but a better way is using
      infinite lists: `take 24 [13,26..]`
    * `cycle` takes a list and replicates its elements and replicates its
     elements indefinitely to form an infinite list
        * make sure to slice it off somewhere if you display the result so it
         doesn't go on forever
         * examples: `take 10 (cycle [1,2,3])` returns `[1,2,3,1,2,3,1,2,3,1
         ]`, `take 12 (cycle "LOL ")` returns `"LOL LOL LOL "` 
    * `repeat` takes an element and produces an infinite list of just that
     element (its like cycling a list with only one element)
* `replicate lengthOfList elementToReplicate` makes a list of a single item
 repeated the specified number of times
    * example: `replicate 3 10` returns `[10,10,10]`
* be careful using ranges with floating point numbers --- can get weird results
    * example: `[0.1,0.3 .. 1]` returns `[0.1,0.3,0.5,0.7, 0.8999999999999,1
    .0999999999999]`
    
## List Comprehensions

* a way to filter, transform, and combine lists
    * very similar to the mathematical concept of *set comprehensions*
        * normally used for building sets out of other sets
        * example of set comprehension:
          {2 * x|x ∈ ℕ, x ≤ 10}
            * means take all the natural numbers less than or equal to 10
            , multiply each one by 2, and use those results to create a new set
    * in Haskell the previous set comprehension example done with list
      comprehensions is `[x * 2 | x <- [1..10]]`
        * we say  we *draw* our elements from the list `[1..10]`
        * `x <- [1..10]` means that `x` takes on the value of each element
         drawn from teh list `[1..10]` --- in other words we *bind* each
          element from the list to `x`
        * the part before the `|` is the *output* of the list comprehension
         --- it is the where we specify how we want the elements we've drawn
          to be reflected in the resulting list --- in this example we want
           each drawn element to be doubled
* basic format: `[output | variable <- listDrawnFrom]`
* predicates are a way to filter the resulting list
    * can have as many predicates as we want separated by commas
* format with predicate: `[output | variable <- listDrawnFrom, predicate]`
    * example: `[x * 2 | x <- [1..10], x * 2 >= 12]` will return only the
     resulting elements that are greater than or equal to 12, `[12,14,16,18,20]`
    * example: numbers from 50 to 100 whose remainder when divided by 7 is 3
      ```
      [x | x <- [50..100], x `mod` 7 == 3]
      ```
    * example: replaces every odd number greater than 10 with "BANG!", overy
     odd number less than 10 with "BOOM!", and throws out numbers that aren't
      odd
      ```
      boomBangs xs = [if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]
      ```
      if call `boomBangs! [7..13]` you get `["BOOM!","BOOM!","BANG!","BANG!"]`
    * example with multiple predicates: `[x | x <- [10..20], x /= 13, x/= 15
    , x/= 19]` is `[10,11,12,14,16,17,18,20]`
* list comprehensions can also draw from multiple lists
    * example: `[x + y | x <- [1,2,3], y <- [10,100,1000]]` returns `[11,101
    ,1001,12,102,1002,13,103,1003]`
    * will take the first element of the first list and apply it to all
     elements in teh second list, then apply the second element in the first
      list to all elements in the second list, etc.
* can use `_` as a temporary variable to store the items as we draw them from
 the list if we don't care about their values
* can create nested list comprehensions
    * example: remove odd numbers without flattening the list
      ```
      ghci> let xxs = [[1,3,5,2,3,1,2,4,5],[1,2,3,4,5,6,7,8,9],[1,2,4,2,1,6,3
      ,1,3,2,3,6]
      ghci> [[x | x <- xs, even x] | xs <- xxs]
      [[2,2,4],[2,4,6,8],[2,4,26,2,6]]
      ``` 
        * a list comprehension always results in a list of something, so fact
         that the output of the outer list comprehension is a list
          comprehension tells us that the result here will be a list of lists
* list comprehensions can be split across several lines to improve their
 readability
