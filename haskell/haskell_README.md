# README

* The notes in this Haskell directory were taken mostly while studying 'Haskell Programming from
 first principles' with a few notes from the beginning of 'Learn You a Haskell for Great Good'.

## GHCi Prompt

* changing prompt: `:set prompt "newPrompt> "`
* can use same command in .ghci file to change permanently
