# misc stuff that needs a place...not sure where want it yet...

# Syntax?

## `Main`

* `main` is the default action when you build an executable  or run it in a REPL
    * it is not a function, but often a series of instructions to execute
    , which can include applying functions and producing side effects
* when building a project with Stack having a `main` executable in `Main.hs
` file is obligatory
* can have source files and load them in GHCi without having a `main` block
* `main` has type `IO ()`
    * IO or I/O stands for input/output
    * it is a special type used when the result of running a program involves
     effects beyond evaluating a function or expression
* printing to the screen is an effect, so printing the output of a module
 must be wrapped in `IO` type
    * when functions are entered directly in the REPL, GHCi implements `IO
    ` without you having to specify
    
## `Do` Syntax

* special syntax that allows for sequencing actions
* most commonly used to sequence the actions that constitute your program
* `do` notation isn't strictly necessary but it makes code more readable than
 alternatives

## 

* define values at the top level of a module so they are available throughout
 the module
* specify explicit types for top-level definitions
    * use `::` to declare types
    * compiler can generally infer types, but it's a good habit
    
## Top-level vs local definitions

* top-level declarations are not nested within anything else
    * they are in the scope of the whole module
    * when compiler reads the file, it will see all the top-level
     declarations, no matter what order they come in (with some limitations)
    * can be exported by the module or imported by a different module
        * cannot import into a different module and reuse top-level names
* a local definition is nested within some other expression and is not visible
 outside that expression
 
## `let` and `where`

* `let` introduces an expression so it can be used wherever you can have an
 expression
* `where` is a declaration and is bound to a surrounding syntactic construct







    
## Type Variables

* some functions can operate on various types
    * example: `head` returns the first element of the list regardless of the
     type that is in the list
      ```
      ghci> :t head
      head :: [a] -> a
      ```   
        * a is not a type because it doesn't start with a capital letter
         --- it is a type variable
* a type variable can be of any type
* they allow functions to operate on values of various types in a type-safe
 manner
* allows us to write very general functions
* functions that use type variables are called *polymorphic functions*
* in practice type variables are often given names like a, b, c, etc though they
 can technically be longer than one character
 
# Type Classes

* Note: type classes are not the same as classes in object-oriented languages
* a *type class* is an interface that defines some behavior --- if a type is an
 *instance* of a type class, then it supports the behavior the type class
  describes
    * it specifies a bunch of functions
    * when we make a type an instance of the type class, we define what those
     functions mean for that type
* example:
  ```
  ghci> :t (==)
  (==) :: (Eq a) => a -> a -> Bool
  ``` 
    * Note: operators are infix functions composed of special characters
     --- if we want to examine its type, pass it to another function, or call
      it as a prefix function we have to surround it in parenthesis
    *  everything before `=>` symbol is called a *class constraint*
        * multiple class constraints can be put in parenthesis and separated
         by commas
    * this type declaration can be read as: The equality function takes any
     two values of the same type and returns a Bool. The type of those
      variables must be an instance of the `Eq` class
        * the `Eq` class provides an interface for testing equality
        * all standard Haskell types (except input/output and functions) are
         instances of the `Eq` class because they can be compared for equality
* a type class defines an abstract interface => one type can be an instance
 of many type classes and one type class can have many types of instances
    * example: the `Char` type is among other things of type `Eq` and `Ord
    ` because we can check if two characters are equal but also compare them
     in alphabetical order
    * some types must first be an instance of one type before they can be an
     instance of another type, like a prerequisite
         
### Common Type Classes

#### Eq Type Class

* used for types that support equality testing
* the functions its instances implement are `==` and `/=`
    * this means that if there's an `Eq` class constraint for a type variable
     in a function, it uses `==` or `/=` somewhere in its definition

#### Ord Type Class

* for types whose values can be put in some order
* example: 
  ```
  ghci> :t (>)
  (>) :: (Ord a) => a -> a -> Bool 
  ```
    * takes two params and returns a Bool
* `Ord` covers all the standard comparison functions `>`, `<`, `>=`, and `<=`
* the `compare` function takes two values whose type is an `Ord` instance and
 returns an `Ordering`
    * `Ordering` is a type that can be `GT`, `LT`, or `Eq` (greater than, less
     than, or equal)
        * examples: 
          ```
          ghci> 5 `compare` 3
          GT
          
          ghci> "Abracadabra" `compare` "Zebra"
          LT
          ```
         
#### Show Type Class

* instances of `Show` class type can be represented as strings
* `show` is the most common function that operates on this type class, it
 prints the given value as a string
    * example: `show 5.334` returns `"5.334"`

#### Read Type Class

* `Read` can be considered the opposite type class of `Show`
* the `read` function takes a string and returns a value whose type is an
 instance of `Read`
    * examples:
      ```
      ghci> read "True" || False
      True
      
      ghci> read "8.2" + 3.8
      12.0
      
      ghci> read "5" - 2
      3
      
      ghci> read "[1,2,3,4]" ++ [3]
      [1,2,3,4,3]
      
      ghci> [read "True", False, True, False]
      [True, False, true, False]
      
      ghci> read "4"
      *** Exception: Prelude.read:no parse
      ```
        * in the last example GHCi is telling us that it doesn't know what we
         want in return
        * we need to do something so it knows what type we want (i.e
        . comparing with `False` in the first example told it we
          wanted a Bool)
* ```
  ghci> :t read
  read :: (Read a) => String -> a
  ``` (`String` is another name for `[Char]`)
    * 'a' can be any type so Haskell has no way to know what type it should be
* use *type annotations* to explicitly tell Haskell what type the
expression should be --- use `::` symbol
    * examples:
      ```
      ghci> read "5" :: Int
      5
      
      ghci> read "5" :: Float
      5.0
      
      ghci> (read "5" :: Float) * 4
      20.0
      
      ghci> read "[1,2,3,4]" :: Int
      [1,2,3,4]
      
      ghci> read "(3, 'a') :: (Int, Char)
      (3, 'a') 
      ```
* the compiler can often infer the type of most expressions by itself, but
 sometimes it can't (i.e. is `read "5"` supposed to return an `Int` or a
  `Float`?) so sometimes we have to tell it --- but as seen in some of the
   earlier examples it can infer type with little information (like how we
    use the result)

#### Enum Type Class

* sequentially ordered types --- their values can be enumerated
    * Definition of enumerate:
      
      *transitive verb*
      * 1: to ascertain the number of : COUNT
      * 2: to specify one after another : LIST
* main advantage of the `Enum` class is that we can use its values in list
 ranges
* `succ` function get the defined successor
    * example: `succ 'B'` returns `'C'`
* `pred` function gets the defined predecessor
* examples of types in this class are: `()`, `Bool`, `Char`, `Int`, `Integer
`, `Float`, and `Double`

#### Bounded Type Class

* instances of `Bounded` type class have an upper and a lower bound
* bounds can be checked using `minbound` and `maxBound`
    * they have a type of `(Bounded a) => a` --- they are like polymorphic
     constants
* Note: tuples whose components are all instances of `Bounded` are
 also considered instances of `Bounded`

#### Num Type Class

* a numeric type class (its instances can act like numbers) that includes all
 numbers 
* can be `Int`, `Integer`, `Float`, or `Double`
* to be an instance of `Num`, a type must already be in `Show` and `Eq`

#### Floating Type Class

* includes `Float` and `Double` types
* functions that take and return values that are instances of `Floating` type
 class need their results to be floating-point numbers in order to do
  meaningful computations (i.e. `sin`, `cos`, `sqrt`)

#### Integral Type Class

* a numeric type class that only includes integral (whole) numbers
* `fromIntegral` function has the type declaration `fromIntegral :: (Integral
 a, Num b) => a -> b`
    * this is useful when you want integral and floating-point types to work
     nicely together
        * example: `length` function returns an `Int` type, so if we want to
         take the length of a list and add it to 3.2 we'd get an error --- we
          can get around this with `fromIntegral (length [1,2,3,4]) + 3.2`
