# Lambda Calculus Basics

## Structure of Lambda Expressions

* lambda terms: expressions, variables, abstractions
* *expression*: superset of all terms...can be a variable name, an abstraction
, or a combo of those things...simplest expression is a single variable
(potential inputs to functions)
* *abstraction*: a function...it has a head (a lambda) and a body and is applied
 to an argument(an input value)
    * the abstraction's head is a 𝛌 (lambda) followed by a variable name
    * the abstraction's body is another expression
    * simple function might look like this `𝛌x.x`
* *variable* named in the head is the parameter; it binds all instances of that
 same variable in the body of the function --- meaning each variable in the
  body of the function will have the value of that argument
  * applying a lambda function to an argument is called *application*
  
### Anonymous Functions
  
* in math we often call functions `f` --- the lambda abstraction `𝛌x.x` has
 no name --- it is an *anonymous function*
    * while it has no name, we call it an abstraction because it is a
     generalization (or abstraction) from a concrete instance of a problem
* it abstracts through the introduction of names
* the names stand for values, but by using named variables, we allow
 for the possibility of applying the general function to different
  values or types of values
* when we apply the abstraction to arguments, we replace the names
 with values, making it concrete
* a named function can be called by name by another function...an anonymous
 function cannot
* basic structure: `𝛌x.x`
    * `𝛌x` is the head of the lambda
    * the first `x` is the parameter
    * the `.` separates the parameters of the lambda from the function body
    * the second `x` is the body, the expression the lambda returns when
     applied --- this is a bound variable

### Alpha Equivalence

* when we write `𝛌x.x` the  `x` is not semantically meaningful except in its
 role in that single expression => there is a form of equivalence between
  lambda terms called *alpha equivalence* 
* alpha equivalence means that `𝛌x.x` = `𝛌d.d` = `𝛌z.z`

## Beta Reduction

* the process of applying a function to an argument by substituting the input
 expression for all instances of bound variables within the body
    * we eliminate the head --- its only purpose was to bind a variable
        * eliminating the head tells us the function has been applied
* example:
  apply `2` to `𝛌x.x` --- `(𝛌x.x)2` = `2`
    * this is the identity function (`f(x) = x` in standard math notation
    ) --- it accepts a single argument `x` and returns the same argument
    * putting parenthesis around the the function to keep clear what is part
     of the function body and what is the input argument
* we can apply the identity function to another lambda abstraction
    * example: `(𝛌x.x)(𝛌y.y)`
        * we will substitute the entire abstraction for `x`
        * the syntax `[x := z]` indicates that `z` will be substituted for
         all occurrences of `x` --- in this example `z` is the function `𝛌y.y`
        * example reduction:
          ```
          (𝛌x.x)(𝛌y.y)
          [x := (𝛌y.y)]
          𝛌y.y
          ```
            * the final result is another identity function
* applications in 𝛌 calc are *left associative* (i.e. it associates, or groups
, to the left)
    * example: `(𝛌x.x)(𝛌y.y)z`, which can be rewritten as `((𝛌x.x)(𝛌y.y))z`
        * example reduction:
          ```
          ((𝛌x.x)(𝛌y.y))z
          [x := (𝛌y.y)]
          (𝛌y.y)z
          [y := z]
          z
          ```
* beta reduction ends when there are no more heads(lambdas) left to apply or
 no more arguments to apply functions to
           
### Free Variables

* purpose of the head is to tell us which variables to replace when we apply
 the function
* free variables are variables in the body, but not bound/named in the head
    * example: in `𝛌x.xy`, `y` is a free variable (and `x` is a bound variable)
        * => the `y` remains irreducible
* example of above abstraction applied to an argument, `z`: 
  ```
  (𝛌x.xy)z
  (𝛌[x := z].xy)
  zy
  ```
* alpha equivalence does not apply to free variables (i.e. `𝛌x.xz` /= `𝛌x.xy
` because `z` and `y` might be different things, but `𝛌x.xz` = `𝛌y.yz
` because the free variable is left alone)
 
## Multiple Arguments

* each 𝛌 can only bind one parameter and can only accept one argument
 --- functions that require multiple arguments have multiple nested heads
* apply it once and the leftmost/first head is eliminated, then the next one
 is applied, etc
    * this is called *currying*
* example: `𝛌xy.xy` is shorthand for two nested lambdas `𝛌x.(𝛌y.xy)`
    * the first argument binds `x`, eliminating the outer lambda --- then we
     are left with `𝛌y.xy` with `x` being whatever the outer lambda was bound
      to
* example:
  ```
  𝛌xy.xy
  (𝛌xy.xy) 1 2
  (𝛌x.(𝛌y.xy)) 1 2
  [x := 1]
  (𝛌y.1y) 2
  [y := 2]
  1 2
  ```
* example:
  ```
  𝛌xy.xy
  (𝛌xy.xy)(𝛌z.a) 1
  (𝛌x.(𝛌y.xy))(𝛌z.a) 1 
  [x := (𝛌z.a)]
  (𝛌y.(𝛌z.a)y) 1
  [y := 1]
  (𝛌z.a) 1
  [z := 1]      # note: no z in functionso there is nowhere to put the 1
  a
  ```
* example with abstract variables (more common):
  ```
  (𝛌xy.xxy)(𝛌x.xy)(𝛌x.xz)
  (𝛌x(𝛌y.xxy))(𝛌x.xy)(𝛌x.xz)
  (𝛌y.(𝛌x.xy)(𝛌x.xy)y)(𝛌x.xz)
  (𝛌x.xy)(𝛌x.xy)(𝛌x.xz)
  ((𝛌x.xy)y)(𝛌x.xz)
  yy(𝛌x.xz)
  ```
* another example with abstract variables:
  ```
  (𝛌xyz.xy(yz))(𝛌mn.m)(𝛌p.p)
  (𝛌x.𝛌y.𝛌z.xz(yz))(𝛌m.𝛌n.m)(𝛌p.p)
  (𝛌y.𝛌z.(𝛌m.𝛌n.m)z(yz))(𝛌p.p)
  𝛌z.(𝛌m.𝛌n.m)z((𝛌p.p)z)        # apply 𝛌m to z
  𝛌z.(𝛌n.z)((𝛌p.p)z)        # apply 𝛌n to ((𝛌p.p)z) --- argument gets tossed
  𝛌z.z                        because there is no n in the function body
  ```
  
## Evaluation is Simplification

* *beta normal form* is when you can't beta reduce the terms any further
    * this is a fully evaluated function (or in programming, a fully executed
     program)
* if the function is "saturated" (it's been applied to all of its arguments
) but you haven't simplified it yet, then it is not fully evaluated, only
 applied
* example: you could apply a function and get `(10 + 2) * 100/2`, but it is
 not simplified and => not in its normal form --- its normal form is `600
 ` because it can't be reduced any further
* example: `𝛌x.x` is fully reduced and => in its beta normal form
* example: `(𝛌x.x)z` is not in beta normal form because the function hasn't
 been applied to the free variable `z` --- it's beta normal form is `z`
     
## Combinators

* a combinator is a lambda term with no free variables --- they combine the
 arguments they are given
    * every term in the body occurs in the head
* these ae combinators: `𝛌x.x`, `𝛌xy.x`, `𝛌xyz.xz(yz)` 
* these are not: `𝛌y.z`, `𝛌x.xz` --- they have free variables

## Divergence

* not all reducible lambda terms reduce to normal form --- they are not fully
 reduced --- they *diverge*
* divergence means the reduction process never terminates or ends
    * reducing terms ordinarily *converge* to beta normal form --- so this is
     the opposite of normal form
* example: (this is a lambda term called omega)
  ```
  (𝛌x.xx)(𝛌x.xx)
  ([x := (𝛌x.xx)]xx)        # the first 𝛌x's x becomes the second lambda 
  (𝛌x.xx)(𝛌x.xx)        # substituting (𝛌x.xx) for each x
  ```     
    * we ended up back where we started, so the reduction process never ends
     and we can say that omega diverges
* in programming this matters because terms that diverge are terms that don't
 produce an answer or meaningful result
     