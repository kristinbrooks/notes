**Deprecated on 12-14-2020**: all notes moved the [kristinruthbrooks.dev repo](https://gitlab.com/kristinbrooks/kristinruthbrooks.dev). They will continue to be updated and added to there.

# notes

Notes on things I'm studying, things I want to remember, etc.
