# Authentication

* complicated and takes a lot of code
    * can use tools to help make it faster, simpler, and with less code

## Tools used in The Web Development Bootcamp

* Passport
    * authentication middleware for Node.js
    * can be used in any Express-based web app
* Passport Local
    * for logging in with a username and password (vs logging in with Facebook or Google)
* Passport Local Mongoose
    * don't have to use, but makes it even easier to use with Mongoose   
* Express-Session 
    
## Sessions

* a little info is sent with every request and saved so http knows when you are logged in (because
 http is stateless)
