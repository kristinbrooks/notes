# Wireframing (low fidelity)

* really basic layout
* one font, one color, etc.
* where you work out the user flow
    * how you get someplace
    * the steps
    * how easy it is
* most important step
* not usually used for testing, generally just for clients
* may sketch out on paper first, just basic outlines to flesh out ideas before start in XD

## How wide for my website?

* based on the product, decide if it is web-first or mobile-first
    * decide based on where most users will use it the most
* for web choose 1920 (this is the width)
    * content will often be within internal boundaries
    * can't make responsive within XD
        * design for the breakpoints, but it won't work til it actually gets coded
* if there isn't an existing website, go [here](https://www.w3schools.com/browsers/browsers_display.asp)
 determine where your boundaries should be 
    * look at the percents where it will look best and consider your user persona to choose the
     most appropriate size
* if the client has an existing website, you can ask for their [google analytics](https://www.analytics.google.com)
    * click audience -> technology -> browser -> find screen resolution 
    * can also go to mobile -> overview and see which devices the users are accessing the product
     from most often
        * can use this info to advise clients if they are pushing the less used type (desktop vs
         mobile) be developed first         

## Working with existing UI kits in XD

* a UI kit is a template
* they make things go fast at the wireframe stage
* if missing fonts from templates try:
    * google fonts
    * adobe fonts
    * or just google search them and try to find a download
