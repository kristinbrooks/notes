# Javascript Conditionals and Loops

## Conditionals

* `if`
* `else if`
* `else`

### Ternary Operator

* `[condition] ? [condition is true] : [the condition is false]

### Switch Statements

* example:
```javascript
let firstName = 'John';
let job = 'teacher';

switch (job) {
  case 'teacher':     // two case staements in a row like this will make the same code
  case 'instructor':  // execute for both of them
    console.log(firstName + ' teaches kids how to code.');
    break;            // stops it from checking other cases
  case 'driver':
    console.log(firstName + ' drives an uber in Lisbon.');
    break;
  case 'designer':
    console.log(firstName + ' designs beautiful websites.');
    break;
  default:            // like an else statement in an if/else
    console.log(firstName + ' does something else.');
}
```
* can replace a long `if`, `else if`,`else if`,`else if`,..., `else` statement --- looks cleaner
    * example:
    ```javascript
    // original if/else if/else statement
    let firstName = 'John';
    let age = 20;
  
    if (age < 13) {
      console.log(firstName + ' is a boy.');
    } if (age >= 13 && age < 20) {
      console.log(firstName + ' is a teenager.');
    } if (age >= 20 && age < 30) {
      console.log(firstName + ' is a young man.');
    } else {
      console.log(firstName + ' is a man.');
    }  
  
    // rewritten as a switch statement
    switch (true) {
      case age < 13:
        console.log(firstName + ' is a boy.');
        break;
      case age < 20:
        console.log(firstName + ' is a teenager.');
        break;
      case age < 20:
        console.log(firstName + ' is a young man.');
        break;
      default:
        console.log(firstName + ' is a man.');
    }
    ```
  
## While Loops

* ```
  while(someCondition){
    // run some code
  }
  ```
* example 1a:
    ```javascript
    let count = 1;
    while(count < 6){
      console.log("count is: " + count);
      count++;
    }
  
    // count is: 1
    // count is: 2
    // count is: 3
    // count is: 4
    // count is: 5
    ```
* example 2a: 
    ```javascript
    let str = "hello";
    let count = 0;
    while(count < str.length){
      console.log(str[count]);
      count++;
    }
  
    // "h"
    // "e"
    // "l"
    // "l"
    // "o"
    ```
* NO writing Infinite Loops -- make sure to increment count or break loop

## for Loops

* ```
  for(init; condition; step){
    // run some code
  }
  ```
* compare the following examples to their while loop equivalents above:
    * example 1b:
        ```javascript
        for(let count = 1; count < 6; count++){
          console.log("count is: " + count);
        }
      
        // count is: 1
        // count is: 2
        // count is: 3
        // count is: 4
        // count is: 5
        ```
    * example 2b:
        ```javascript
        let str = "hello";
        for(let i = 0; i < str.length; i++){
          console.log(str[i]);
        }
      
        // "h"
        // "e"
        // "l"
        // "l"
        // "o"
        ```

## Continue and Break 

* ```javascript
  var john = ['John', 'Smith', 1990, 'designer', false, 'blue'];
  
  for (var i = 0; i < john.length; i++) {
      if (typeof john[i] !== 'string') continue;
      console.log(john[i]);
  
  // John
  // Smith
  // designer
  // blue
  }
  ```
    * if the element is not a string the loop will just continue to the next iteration
* ```javascript  
  var john = ['John', 'Smith', 1990, 'designer', false, 'blue'];
  
  for (var i = 0; i < john.length; i++) {
      if (typeof john[i] !== 'string') break;
      console.log(john[i]);
  // John
  // Smith
  }
  ```
    * if the element is not a string the loop will terminate

## Looping Backwards

* ```javascript
  var john = ['John', 'Smith', 1990, 'designer', false, 'blue'];
  
  for (let i = jihn.length - 1; i >= 0; i--) {
    console.log(john[i]);
  
  // blue
  // false
  // designer
  // 1990
  // Smith
  // John
  }
  ```
