# Node

* Node.js is a javascript runtime built on Chrome's V8 javascript engine. it uses an event-driven
, non-blocking I/O model that makes it lightweight and efficient. (from nodejs.org)
* allows us to write javascript code on the server side/backend

## Interact with Node Console

* has its own version of the console --- on command line
* not that useful
* it's a REPL
* `ctrl-C ctrl-C` to quit
* can't use some commands you can use in the javascript console can't be used here (DOM commands
, `alert()`, etc) because they are browser/frontend based

## Run a File with Node

* create a js file and execute it on the command line with `node fileName.js`
    * can print `console.log()` outputs in the terminal
* to run more complicated things that are running on servers and such use `node app.js` command
    * it will tell you it is running on port 3000, which is where you can view the app in the
     browser
     
## npm

* nodes javascript package manager
* can't include packages with script tags like with libraries because the server side doesn't use
 html
* an alternative is yarn -- another package manager that was created by Facebook
    * can use either package manager, but make sure to stick with the same one for a whole project

### Installing packages and Using Packages

* `npm install` or `yarn install` to install a package
* install in project root
* in your `.js` file use format: `let variableName = require('packageName');`
* then you will call methods with `variableName.methodFromPackage();`

### Package.json and NPM Init

#### `package.json`

* All npm packages contain a file, usually in the project root, called `package.json` - this
 file holds various metadata relevant to the project. This file is used to give information to
  `npm` that allows it to identify the project as well as handle the project's dependencies. It
   can also contain other metadata such as project description, the version of the project in a
    particular distribution, license information, even configuration data - all of which can be
     vital to both `npm` and to the end users of the package. The `package.json` file is normally
      located at the root directory of a Node.js project. --- this description was from [docs
      .nodejitsu.com](http://docs.nodejitsu.com/articles/getting-started/npm/what-is-the-file-package-json), which doesn't exist anymore
* `json` stands for javascript object notation
* contains a list od all the dependencies needed to run the app, but doesn't download them
 automatically
    * json dependencies analogy paraphrased from Colt Steele: Suppose you have a great recipe and
     you want your friend in Denmark to be able to replicate it. One option would be to buy all the
      ingredients, measure them, package them in bags, and then put in a box with instructions
       and ship it to the friend. Alternately, you can send them instructions with a description
        of all the ingredients and their quantities and send that. Then friend can just prepare
         it on their own. The `package.json` is like the mailed recipe. It will list all the
          dependencies and then the user can go and install those packages themselves.

#### `npm init`

* used to create a `package.json` file inside the directory where the application will exist
* it will ask questions and use the provided data to make the `package.json` file for us at the end
    * it will put its guesses at the answers in parenthesis ---  if that is what you want you can
     just hit enter
    * won't have any dependencies yet
* to add dependencies install a package with the `--save` flag
    * flag automatically saves the package name and version to the `package.json` dependencies list
* `npm init -y` - automatically fills in `package.json` with what it thinks should be there    

## Automate the Restarting of the Node Server

* keeps you from having to stop and restart your Node server manually everytime you edit your code
* use Nodemon
* not a package that is installed to your project
* run `npm i -g nodemon` to install
* global install allows us to use anywhere on our computer with any project
* run `nodemon` command in node console to use --- it will run in the background and restart the
 server automatically
