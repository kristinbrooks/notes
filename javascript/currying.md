# JavaScript Currying

* **NOTE**: can play with working code examples in the repo: 
 `learning-functional-javascript-with-ramda/basic_knowledge/currying`

* used to write cleaner and more elegant code
* normally a function is created like this:
    ```javascript
    let add = function (a, b) {
      return a = b;
    }
  
    add(1, 2);      // 3
    ```
* extra arguments can be added without throwing errors (i.e. `add(1, 2, 'ignore');`) because the
 extra argument is just ignored
* if we leave out an argument (i.e. `add(1)`) we get `NaN` which means 'Not a Number'
* a curried function won't do anything until it gets enough arguments

## Benefits

* we can split functions into small pieces that are easy to reuse
* 
