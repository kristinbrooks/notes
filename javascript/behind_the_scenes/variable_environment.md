**NOTE**: All images from Jonas
Schmedtmann's [The Complete Javascript Course 2020](https://www.udemy.com/course/the-complete-javascript-course/)

# Execution Context: Variable Environment

![Execution Context: Variable Environment](images/ve1.png)

## Hoisting and the TDZ

![Variable Environment](images/ve2.png)

![Variable Environment](images/ve3.png)

## Code Examples

### Hoisting Variables

* at the point where we `console.log` the variables, `var` is undefined, while `let` and `const` are uninitialized
  (they are in the TDZ)
    * ![Hoisted Variables](images/ve4.png)
* `var` creates a property on the global window object, while `const` and `let` do not
    * ![Hoisting Example](images/ve10.png)

### Hoisting Functions

* only the regular function `addDecl` can be accessed before it occurs in the code
* here `addExpr` and `addArrow` are defined as `const`s so they are in the TDZ
    * ![Hoisting Functions](images/ve5.png)
* here `addExpr` and `addArrow` are defined as `var`s so they are undefined at the time they are called, so they can't
  be called or accept arguments
    * ![Hoisting Functions](images/ve6.png)
    * ![Hoisting Functions](images/ve7.png)

### Be Careful

* `numPoducts` is undefined, which is a falsy value, so it enters the if statement even though it hasn't been
  initialized yet
    * ![Hoisting Example](images/ve8.png)
    * this kind of thing would be a hard big to find in a large codebase
    * best practices for clean code (according to Jonas):
        * try to always use `const` if possible, then `let` if you really need to change it later
        * declare all variables at the beginning of their scope
        * declare all functions first and only use them after they are declared (even function declarations)
