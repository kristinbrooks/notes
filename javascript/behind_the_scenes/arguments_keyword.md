**NOTE**: All images from Jonas Schmedtmann's [The Complete Javascript Course 2020](https://www.udemy.com/course/the-complete-javascript-course/)

# `arguments` Keyword

* it's not used that much in modern JS, but it's still good to know about it
* regular functions, like `addExpr` get an `arguments` keyword and arrow functions don't (just like with the `this` 
  keyword)
    * ![Arguments](images/argument1.png)
* it is completely valid to add more arguments than there are parameters, they are just unnamed
    * ![Arguments](images/argument2.png)
* arrow functions, like `addArrow` do not have the `arguments` keyword
    * ![Arguments](images/argument3.png)
