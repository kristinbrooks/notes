**NOTE**: All images from Jonas
Schmedtmann's [The Complete Javascript Course 2020](https://www.udemy.com/course/the-complete-javascript-course/)

# JavaScript Engine

![JavaScript Engine](images/js-engine1.png)

![Compilation, Interpretation, Just-in-time Interpretation](images/js-engine2.png)

![Justin-time Compilation](images/js-engine3.png)
* [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree) - abstract syntax tree

![Just-in-time-compilation](images/js-engine4.png)

![Runtime in the Browser](images/js-engine5.png)

![Runtime in Node.js](images/js-engine6.png)
