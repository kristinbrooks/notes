**NOTE**: All images from Jonas
Schmedtmann's [The Complete Javascript Course 2020](https://www.udemy.com/course/the-complete-javascript-course/)

# Execution Context: Scope and the Scope Chain

![Execution Context - Scope Chain](images/scope1.png)

## Scope

![Scope](images/scope2.png)

![Scope](images/scope3.png)

## The Scope Chain

![Scope Chain](images/scope-chain1.png)

![Scope Chain](images/scope-chain2.png)

### The Scope Chain and the Call Stack

![Scope Chain](images/scope-chain3.png)

![Scope Chain](images/scope-chain4.png)

![Scope Chain](images/scope-chain5.png)

![Scope Chain](images/scope-chain6.png)
