# Package Managers

## Python Project Manager: PIP

* includes:
    * the files that make up the actual program
    * metadata: data about the data
    * dependencies: the programs it relies on to run
* package manager will download a package for you and install its dependencies so you can use it
* after installed you can import it as a module in a program
* to see if installed enter `pip3` on the command line
    * will see list of options
* `sudo pip3 install package_name` - to install
* `sudo pip3 uninstall package_name` - to uninstall
* packages will be installed in 'site-packages' directory in 'python'directory
* `pip freeze` - shows all installed packages
* installing packages into a virtual environment allows you to keep the packages for different projects separate
