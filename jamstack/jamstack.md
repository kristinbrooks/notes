# 'Jamstack 101 and Beyond with Netlify' (Jamstack Conference Workshop Oct. 2020)

[workshop video](https://drive.google.com/file/d/1K5WvIWmvSxfvWS7rk5jCOkEL0rmwOq3-/view)

## What is the Jamstack?

* the Jamstack is not, in fact, a tech stack --- it is an architecture for building modern websites
    * can build a Jamstack site with different tools, doesn't need to be specific programming language, database, etc
    * it is a way of arranging the code in apps so that they follow an overarching guideline/philosophy for
     application development

## What makes an app 'Jamstack'?

* cacheable, static assets are pre-rendered (i.e. build-time rendering) whenever possible
* building assets on the fly (i.e. request-time rendering) is avoided whenever possible
* assets are hosted on a content delivery network (CDN)
    * "A content delivery network (CDN) refers to a geographically distributed group of servers which work together
     to provide fast delivery of Internet content." from [here](https://www.cloudflare.com/learning/cdn/what-is-a-cdn/) 
     --- there is much more info on the page
    * CDNs are the most highly tested scalable pieces of software we have globally right now --- if they go down like
     the whole internet goes down (AWS, Google Cloud, etc.)
* dynamic interaction is handled through asynchronous JavaScript 
     
## Isn't this server-side rendering?

* server-side rendering --- taking a framework like angular, react, or vue and doing work on the server-side so that
 what you are sending to the browser isn't the react code, but the rendered html that the react code would generate
* SSR can mean 2 different things:
    1. **request-time rendering** (probably the most common understanding of SSR)
        * example: put react on a node server and whenever somebody requests a page you would generate the react on
         the node server and then send the result at request time
        * can let you include user data, customize the page --- powerful --- but also must redo the work every time
         someone requests the page --- get DDOS attacks
        ![Request Time Rendering Diagram](images/requestTimeRendering.png "Request Time Rendering Diagram")
        ![Request Time Rendering Server Failure Diagram](images/rtrFailure.png "Request Time Rendering
         Server Failure Diagram")
        ![Request Time Rendering Database Failure Diagram](images/rtrFailure2.png "Request Time Rendering
         Database failure Diagram")
    1. **build-time rendering**
        * don't load at every request --- do the work once --- build files/render and put that on the CDN
        * can help reliability and scalability
        ![Build Time Rendering Diagram](images/buildTimeRendering.png "Build Time Rendering Diagram")
        ![Build Time Rendering Server Failure Diagram](images/btrServerFailure.png "Build Time Rendering Server
         Failure Diagram")
         
## Netlify Drag & Drop

* [drag and drop](https://app.netlify.com/drop) a folder with a site's html, css, and js files --- they will be rendered live
    * this is an ideal jamstack site --- just a file with code --- no build step required
* if need to update site, just fo to the 'deploys' tab and drag & drop the folder again  
* if want to go back to a previous version, click on the deploy you want to use and click 'Publish deploy'
* each deploy is a separate folder (on the Netlify site, not necessarily on your computer), so can go and look at
 anything in the past
 
## Netlify CLI

* `npm install netlify-cli -g`
* then you can run commands with `netlify` or `ntl`

 
     