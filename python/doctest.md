# Python doctest

* format:   
  ```
  """
  Any notes about function here...
  ...then doctest/s:
  >>> function_name(argument)
  what the expected result is
  """
  ```
* `python3 -m doctest file_name.py -v` - runs the doctests for file_name.py from command line
  * `-m doctest` calls the doctest module
  * `-v` makes it verbose mode
  * see links under 'Testing Info/Resources' for other ways to run the doctests
