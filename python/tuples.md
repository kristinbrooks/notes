# Python Tuples

* a kind of container along with lists, and dictionaries
* similar to lists, but they are immutable (can't be changed)
* `tuple()`
* if the tuple only has one item, it still needs a comma i.e (1,)
* python will raise an exception if you try to change it
* get elements with `tuple_name[index]`
* check if item is in tuple with `in` keyword
* can be used as keys in dictionaries
