# Python Variables Passed as Parameters

## immutable

* you get copies of values for parameters (pass-by-value)
* the variable has not been changed
* these are primitive types
* examples: integer, float, Boolean, string

## mutable

* these variables act like they have a URL that is being copied (pass-by-reference)
* you get reference (the URL) for parameters 
* they are changed in place when you assign
* non-primitive types
* examples: canvas, pixel, SimpleImage, list
