# APIs

* allow you to connect to other applications
* stands for Application Programming Interface
    * interface for code/computers to talk to one another
    * from [wikipedia](https://en.wikipedia.org/wiki/Application_programming_interface): '...is a
     set of routines, protocols, and tools for building software and
     applications'
* Web APIs generally communicate via HTTP --- these are what most people mean when they just say API
* can have non web APIs for things like connecting to hard drives, video cards, etc.

## Web APIs

### Data Formats

* when we use the internet, we make an HTTP request and get HTML back
* APIs don't respond with HTML because that is the info about the structure of a page --- they
 respond with data
* they use simple data formats like **XML** and **JSON**

#### XML

* Extended Markup Language
* syntactically similar to HTML, but it doesn't describe presentation like HTML
* example:
    ```xml
    <person>
      <age>21</age>
      <name>Travis</name>
      <city>Los Angeles</city>
    </person>
    ```
* it just describes key/value pairs, can be nested
* can put whatever you want in the tags

#### JSON

* Javascript Object Notation
* JSON looks exactly like Javascript objects, but everything is a string
* example:
    ```json
    {
      "person": {
        "age": "21",
        "name": "Travis",
        "city": "Los Angeles"
      }
    }
    ```
* has become much more popular than XML because you are often calling info in javascript
 --- since JSON is very similar to javascript it is easier to quickly use the data you get back
    * most likely to find XML in older APIs --- very rare in anything new
* usually shorter than XML    
 
### Making API Requests with Node

* `curl` is a way to make a request from the terminal
* in apps for The Web Developer Bootcamp, we will use either the 'axios package' or the 'request
 package' via Node 
* axios is a *promise* based package
* see repo `the-web-developer-bootcamp/12_api/axios_lesson` for examples with notes

## RESTful API (REST API)

* REST stands for REpresentational State Transfer, which is an architectural style and approach
 to communications often used in web services development
* REST is a mapping between HTTP routes and CRUD --- it is a pattern of routes
* following RESTful route conventions makes code reliable because everyone interacting with it
 will understand the architecture/patterns it follows --- but it is just a convention, so you can
  break it if you want 

### RESTful Routes

example table of the 7 RESTful routes:
```markdown
Name         url/path         HTTP verb      Mongoose Method           Purpose
                                       
==================================================================================================
INDEX        /dogs            GET            Dog.find()                Display a list of all dogs
--------------------------------------------------------------------------------------------------
NEW          /dogs/new        GET            N/A                       Show new dog form
CREATE       /dogs            POST           Dog.create()              Create a new dog, then
                                                                                redirect
--------------------------------------------------------------------------------------------------
SHOW         /dogs/:id        GET            Dog.findById()            Shows info about one
                                                                                specific dog
--------------------------------------------------------------------------------------------------
EDIT         /dogs/:id/edit   GET            Dog.findById()            Show edit form for one dog
UPDATE       /dogs/:id        PUT            Dog.findByIDAndUpdate()   Update a particular dog, 
                                                                                then redirect
--------------------------------------------------------------------------------------------------
DESTROY      /dogs/:id        DELETE         Dog.findByIdAndRemove()   Delete a particular dog, 
                                                                                then redirect
```
* CRUD:
    * Create routes: NEW, CREATE
    * Read routes: INDEX, SHOW
    * Update routes: EDIT, UPDATE
    * Destroy routes: DELETE
* the 3 routes that redirect are the 3 non-GET requests
* SHOW/UPDATE/DESTROY and INDEX/CREATE have the same paths/urls, but they are very different
 because of their different HTTP verbs (GET/PUT/DELETE and GET/POST)
* see examples in repo: `the-web-developer-bootcamp/14_RESTful_blog_app_project`

### Nested Routes

* want to add comments to specific dogs --- but can't just add /comments/new and /comments to
 achieve this because those aren't associated to a dog 
* example table showing the original NEW/CREATE routes and the new NESTED NEW/CREATE routes to
 add comments:
```markdown
Name       url/path                 HTTP verb     Mongoose Method       Purpose
                                       
==================================================================================================
INDEX      /dogs                    GET           Dog.find()            Display a list of all dogs
--------------------------------------------------------------------------------------------------
NEW        /dogs/new                GET           N/A                   Show new dog form
NEW        /dogs/:id/comments/new   GET                                 Show new comment form
CREATE     /dogs                    POST          Dog.create()          Create a new dog, then
                                                                              redirect
CREATE     /dogs/:id/comments       POST          Dog.findById()        Associate with a dog
                                                  Comment.create()      Create a new comment
                                                                        Save them both 
--------------------------------------------------------------------------------------------------
SHOW       /dogs/:id                GET           Dog.findById()        Shows info about one
                                                                              specific dog

```
