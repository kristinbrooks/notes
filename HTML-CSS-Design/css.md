# CSS

**NOTE**: there are some examples with notes in the-web-developer-bootcamp repo --- and more examples in
build-responsive-real-world-websites-with-html5-css3 repo 

**NOTE**: images are screenshots from [Build Responsive Real World Websites with HTML5 and CSS3](https://www.udemy.com/course/design-and-develop-a-killer-website-with-html5-and-css3/) 
by Jonas Schmedtmann

* **C**ascading **S**tye **S**heets
* defines exactly how the HTML should look
* can be written in 3 places:
    * inside an HTML tag (in index.html)
        ```html
        <p style="font-size: 120%"></p>
        ```  
    * inside an HTML document (in index.html)
        ```html
        <style>
            p {
                font-size: 120%
            }
        </style>
        ```
    * in an external file (in style.css)
        ```css
        p {
            font-size: 120%;
        }
        ```
      
## Format

* css is written in rules
    * each rule contains a **selector** and a **declaration block**
* ```css
  selector {
      /*declaration block*/
  }
  ```
* child elements inherit styles from their parent elements (e.g. an h1 will inherit from body) unless we override
 their styles
    * the most specific selector will be the one displayed
* the `*` selector affects everything    
  
## Color

* the RGB model --- each color can range from 0 (hex 0) to 255 (hex ff) 
    * colors written using hex notation look like `#RRGGBB`
    * colors can be transparent
        * don't use hex notation --- use `rgba(R, G, B, transparency percent` (0 is completely transparency, 1 is opaque)

![RGB Color Model](images/rgb.png "an illustration of the RGB Color Model")    

## Classes and IDs

* classes can be used on as many elements as we want
* ids can only be used once
    * using ids is considered bad practice because they can only be used once --- usually use classes for all styling
     even if there is only one element with the class --- this allows for easier changes in the future
     
## The Box Model

![The CSS Box Model](images/boxModel.png "The CSS Box Model")

* with `box-sizing: border-box`, the height and width of the entire box can be defined rather than just for the content
    * there are block, inline, and inline-block elements
        * block
        ![Block Elements](images/blockElements.png "Block Elements")
            * use the full width of the browser
        * inline
        ![Inline Elements](images/inlineElements.png "Inline Elements")
            * can set height and/or width
        * inline-block
            * doesn't have a line-break like a regular inline element, but can have margin and padding like a block
             element

## Relative vs. Absolute Positioning

* relative --- position determined by other elements in the page
* absolute --- can be positioned anywhere inside their parent element
     * the parent element's position must be set to relative
