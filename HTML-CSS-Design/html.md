## HTML

**NOTE**: there are some examples with notes in the-web-developer-bootcamp repo --- it was a code along style class, so
 it has many examples and I added clarification notes where I felt I needed them
 
**NOTE**: more examples in build-responsive-real-world-websites-with-html5-css3 repo 

* **H**yper **T**ext **M**arkup **L**anguage
* HTML documents are described by HTML tags
    * syntax: `<tagname>content</tagname>`
     
### Structure of HTML Document

* `index.html` is the standard name of the main html file of any project
* ```html
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8">
      <title>Title</title>
    </head>
    <body>
      
    </body>
  </html>
      ```
* attributes define additional characteristics or properties of the element
    * they are always defined in the starting/opening tag
    * usually consists of name/value pairs (e.g. `name:"value"`)
        * attribute values are always enclosed in quotation marks
    * some tags have required attributes (e.g. and `<img>` tag needs `src` and `alt` attributes)
* `<div></div>` is one of the most used tags --- stands for divide --- used to separate our content into different
 sections
  
     