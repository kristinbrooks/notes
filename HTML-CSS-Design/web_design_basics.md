**NOTE**: These notes are based on Section 4 in [Build Responsive Real World Websites with HTML5 and CSS3](https://www.udemy.com/course/design-and-develop-a-killer-website-with-html5-and-css3/) 
by Jonas Schmedtmann --- so they are his design opinions :) --- images are screenshots from his lectures

# Web Design Basics
 
* a designer creates the look and feel, while the developer builds it --- this line is becoming more blurred
    * 'flat design' has made it easier for beginner to designers
    
        ```markdown

        DESIGN CHEAT SHEET
        
        Beautiful Typography
        
            1. Use a font-size between 15 and 25 pixels for body text
            
            2. Use (really) big font sizes for headlines
            
            3. Use a line spacing between 120 and 150% of the font size
            
            4. 45 to 90 characters per line
            
            5. Use good fonts
            
            6. Chose a font which reflects the look and feel you want for your website
            
            7. Use only one font
        
        Using Colors Like a Pro
        
            1. Use only one base color
            
            2. Use a tool if you want to use more colors
            
            3. Use color to draw attention
            
            4. Never use black in your design
            
            5. Choose colors wisely
        
        Working with Images
        
            1. Put text directly on the image
            
            2. Overlay the image
            
            3. Put your text in a box
            
            4. Blur the image
            
            5. The floor fade
        
        Working with icons
        
            1. Use icons to list features/steps
            
            2. Use icons for actions and links
            
            3. Icons should be recognizable
            
            4. Label your icons
            
            5. Icons should not take a center stage
            
            6. Use icon fonts whenever possible
        
        Spacing and layout
        
            1. Put whitespace between your elements
            
            2. Put whitespace between your groups of elements
            
            3. Put whitespace between you website's sections
            
            4. Define where you want your audience to look first
            
            5. Establish a flow that corresponds to your content's message
            
            6. Use whitespace to build that flow    
        ```
    
## Typography

* "Typography is the art and technique of arranging type to make written language legible, readable, and appealing
 when displayed." [Wikipedia 10/6/20](https://en.wikipedia.org/wiki/Typography)
* really important because generally most of the website will consist of text --- it will affect how professional the
 website looks
* **body text** - use font size between 15 and 25 px
* **headlines** - use really big font sizes --- no specific size limits --- may need to decrease the font weight so
 the text isn't too heavy
* **line spacing** - the vertical distance between lines to make it easier to read --- use between 120 and 150%
* **line length** - 45 to 90 characters per line is optimal for ease of reading (can be less if there are columns)
* use good fonts :)

### Sans-serif vs. Serif 

![Sans-Serif vs. Serif Fonts](images/fonts.png "Sans-serif vs. Serif Fonts")

* good sans-serif fonts:
    ![Good Sans-Serif Fonts from Google web fonts](images/sans_serif.png "Good Sans-Serif Fonts from Google web fonts")
* good serif fonts:
    ![Good Serif Fonts from Google web fonts](images/serif.png "Good Serif Fonts from Google web fonts")

### Choosing a Font

1. Choose a font which reflects the look and feel you want for your website
1. Decide: sans-serif or serif typeface
1. Use a good font
1. Use only that one typeface (if you're a beginner)

* play around!!! to see what you like and what works for your design

## Colors

* probably most complicated part of UI design
* bad choices can be distracting and/or make the design look very messy
    * example of REALLY bad color (and clutter)
        ![Really Bad USe of Color and Distracting Design](images/really-bad-color.png "Really Bad Use of Color and
         Distracting Design")
* Color Tools:
    * [Flat UI Colors](https://flatuicolors.com/) --- a palette resource with nice choices for base colors and
     coordinates
    * [coolors](https://coolors.co/) - awesome palette maker --- can make gradients between 2 colors, see how it
     will look to color blind people, etc
    * there is also a built in color tool in Adobe programs
* best to use **one base/main color** --- any color that is not black, white, or grey
    * using too many colors in too many places can ruin a website trying to be clean and simple (especially for
     beginners)
* use main color to draw attention to the most important elements on page
* use obvious colors that stand out to draw attention to important buttons sand such on your website
* don't use black --- pure black almost never occurs in the real world so it doesn't look natural

### Psychology of Color

* you want to pick a color that gets the desired response from your audience
* **RED** - power, passion, strength, excitement
    * brighter tones are more energetic --- darker shades are more powerful and elegant
* **ORANGE** - cheerfulness, creativity, friendliness, confidence, courage
    * draws attention without being as overpowering as red
* **YELLOW** - energetic, happiness, liveliness, curiosity, intelligence, brightness
* **GREEN** - harmony, nature, life, health, money
    * can have balancing and harmonizing effect in design
* **BLUE** - patience, peace, trustworthiness, stability, professionalism, trust, honor
    * one of most beloved colors --- especially by men
* **PURPLE** - power, nobility, wealth, wisdom, royalty, luxury, mystery
* **PINK** - romance, peace, affection, passivity, care
* **BROWN** - relaxation, confidence, earthiness, durability, comfort, reliability, nature

## Images

* large images can create more user engagement
* so we put text over images
* they need enough contrast or the text isn't readable
* options to make text pop over images
    * the easiest solution the contrast issue is to overlay the image with a color to make it darker --- black is
     simplest, but can use other colors or gradients, etc.
    * put text in a mostly opaque box over the image to make it stand out
    * use an image blur --- whole image --- or just part of the image --- need to make sure the text stays over the
     blurred part on all screen resolutions
    * the floor fade --- image fades to black at the bottom of the image (text over the faded part at the bottom) 

## Icons

* can help users to quickly browse through the page
* to list features/steps
* for actions and links
    * icons need to be recognizable
    * label icons 
* icons should not take center stage in the design
* use icon fonts/ vector images when possible

## Spacing and Layout

* need whitespace to have a well designed website:
    * put whitespace between your elements
    * put whitespace between groups of elements
    * put whitespace between your website's sections
    * don't exaggerate or things will look disconnected and lose their relationships to each other
* define visual hierarchy --- they give your website order
    * decide what you want the audience to look first
    * establish a flow that corresponds to your content's message
    * use whitespace to build that flow
 
## User Experience (UX)

* user interface is the presentation of a product, how it looks and feels --- that is what all the previous sections
 are about
* **user experience** is the overall experience the user has with a product  --- it includes the user interface, but
 also has to understand the whole picture of the product
        ![Designing the Product vs. Designing the Experience](images/ux.png "Designing the Product vs. Designing the Experience")
      
## Inspiration

* use other websites as inspiration to see what other leading designers are doing right:
* collect designs you like
* try to understand everything about them:
    * why do they look good
    * what do they have in common?
    * how were they built in HTML and CSS?
* steal like an artist (obviously don't literally steal) --- in the beginning don't worry about being too original, it
 will come with time    
* [Dribble.com](https://dribbble.com/) is a good inspiration site 
