**NOTE**: These notes are based on Section 5 in [Build Responsive Real World Websites with HTML5 and CSS3](https://www.udemy.com/course/design-and-develop-a-killer-website-with-html5-and-css3/) 
by Jonas Schmedtmann --- so they are his design opinions :) 

# 7 Steps to a Fully Functional Website

**Never start designing without having an idea of what you want to build.**

1. Define the goal of the project (e.g. building a blog, making portfolio, etc.), your audience --- design with the
 goal and audience in mind
2. Plan out everything:
    * content (text, images, videos, icons, etc) --- called content first approach
    * start thinking about visual hierarchy
    * define the navigation
    * sitemap if it's a bigger project
3. Sketch your ideas before you design
    * get inspired and think about your design
    * get the ideas out of your head (sketch)
    * make as many sketches as you want, but don't make it perfect --- can work out fine details later
4. Design and develop your website --- this is the biggest part of building a website
    * do that using HTML and CSS --- called 'designing in the browser' --- saves time and makes it easier to design a
     responsive website
    * use content and planning decisions from steps 1,2, and 3
    * Make appearance consistent on all browsers
        * browser prefixes
            ![Browser Prefixes](images/browserPrefixes.png "Browser Prefixes")
            
            * Note: in Brackets, use autoprefixer extension: `Edit -> Auto prefix selection`
        * can use JS scripts to make things work properly in older browsers
        * [Can I Use](https://caniuse.com) to see what is supported in each browser
5. Optimization
    * **optimize performance** (site speed)
        * reduce image sizes
            * make sure to find the image size when it is its largest size due to the site being responsive
            * if not inspecting on a high resolution screen, size the image twice as big to account for that
            * can resize easily in preview on a mac
            * [Optimizilla](https://imagecompressor.com/)
        * minify code
    * **search engine optimization** (SEO) --- techniques that improve and promote a website to increase the number of
     visitors the site receives from search engines
        * meta description tags
        * validate html code --- [W3C Markup Validation Service](https://validator.w3.org/)
        * content is king
        * keywords
        * backlinks (links on other sites that point to your site)
6. Launch the website
    * choose and buy a domain name
    * buy web hosting
    * upload website
7. Site maintenance
    * monitor your users behavior and success of website
        * Google Analytics
            * setup your site and add the provided script to your HTML file --- then you can look at all your statistics
    * update content regularly
    
[Periodic Table of Web Design](https://www.newdesigngroup.ca/blog/web-design-process-infographic)    
  