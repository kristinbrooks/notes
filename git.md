# Git

(note: see command_line notes to find info on VIM)

* version control system
* `HEAD` - currently checked out --- the files editable on my machine/working copy/working space

## Help

* `git command --help` - to get into help menu within git
* [Detailed Reference/Docs](https://git-scm.com/docs)
* [Interactive git examples](https://onlywei.github.io/explain-git-with-d3/)
* [GitHub cheat sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf)
* [GitLab cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
* [Interactive visual cheat sheet](https://ndpsoftware.com/git-cheatsheet.html)

## General

* when creating new github repository include `.gitignore` directory - this allows us to avoid committing unnecessary
files that would commit every time - manually add `.idea` and anything else you want ignored to
the `.gitignore` directory
* `cat .git/config` ` look at remote to see where it is pointed
* `git config [option]` - to change your config (can also do it in an editor to the .git/config file)
    * new github/gitlab account requires 1 time config setup of email and username

## Status

* `git status` - shows which files need to be to be staged, committed, etc (git status alias `gst`)

## Log

* `git log` - shows changes
* `git log --graph` - shows paths
* `git log --name-status` - only shows names of files changed in a commit
    * will have `A`, `M`, or `D` at the beginning of lines to show if the file was added, modified, or deleted
* `git log -p` - shows all changes
* `q` - to quit log
* `git reflog` - shows history of all the refs(places your head has changed to)

## Making Repositories

* `git init` - initializes empty git repository (easier to make new repository on github/gitlab first and then clone it)
* `git remote add origin https:/...` - connect repository to a blank github repository at the address "https:/..."
* `git push --set-upstream git@gitlab.com:kristinbrooks/new-project-name.git master` - makes a new project on gitlab and
pushes my local repository to master
* `git clone https://...` - clone new github/gitlab repository with address "https://..."

## Checkout

* `git checkout branch_name` - checkout the "branch_name" branch from github/gitlab (checkout alias `co`)
* `git checkout .` - reverts committed files that have been modified (checkout alias `co`)

## Remote Tracking Branches

* `git remote -v` - shows a list of the local remote tracking branches/clones

## Diffs (differences)

* `git diff` - shows changes between the working tree and the index or a tree, changes between the index and a tree,
changes between two trees, changes between two blob objects, or changes between two files on disk
    * this much easier to follow in the IDE
* `git diff --name-status` - show only the names and status of the changed files

## Branches

* `git branch new_branch_name` creates a new branch, but does not switch to it
* `git sw name_of_branch_want_to_switch_to` - switch branches (old way is `git co branch_name`)
* `git sw -c new_branch_name` - create new branch and switch to it (switch alias `sw`)
* `git cherry-pick commit_sha` - puts the sha on the tip of a new branch 
    * if doing this make sure remaining commit/s on original branch are still intact

## Pulling

* `git pull` - runs `git fetch` and calls `git merge` to merge the retrieved branch heads into the current branch
* `git pull --rebase` - fetches and then rebases instead of merging

## Fetching

* `git fetch` - gets the updated versions of the remote tracking branch/es
    * if no remote is specified the default `origin` remote will be used
* `git fetch --all` - gets all remote tracking branches for the repo

## Stashing

* `git stash -u` - stashes local changes
* `git stash pop` - pops the changes from the stash

## Adding

* `git add filename`- stages file "filename"
* `git add .` - stages current directory

## Committing

* `git commit` - commits and opens VIM (commit alias `ci`)
* `git commit -m "notes about commit here"` - commits without opening VIM
* `git commit --amend` - edit last commit, VIM will open to edit commit message
* `git commit --amend --no-edit` - edit last commit without changing commit message
* [Writing good commit messages](https://github.com/erlang/otp/wiki/Writing-good-commit-messages)

## Rebasing and Merging

* `git rebase` - sets aside your changes, updates your local branch to be up to date, then applies your changes on top
    * if rebase, must force push afterwards
* `merge branch_name --no-ff` - this is a merge commit, it only holds meta data not the actual changes
    * rebasing blows away any merge commits that happened after you branched off

## Pushing

* `git push` - pushes changes to github/gitlab
    * `origin` is the default push location if no location is provided
* `git push --force-with-lease` - push amended/rewritten history (using just `--force` will blow away any changes if
 you're not up to date, so always use `--force-with-lease` to be safe)
* `git push -u` - makes a new remote tracking branch
* `git push --set-upstream` - does the same thing as `git push -u`

## Cleaning

* `git clean -df` - get rid of unstaged files that have never been committed
* `git clean -dfx` - also clean gitignored files

## Resetting

* `git reset --hard <upstream/master, origin/master>` - Resets the index and working tree. Any changes to tracked files in the working tree since <commit> are discarded [reset docs](https://git-scm.com/docs/git-reset)
